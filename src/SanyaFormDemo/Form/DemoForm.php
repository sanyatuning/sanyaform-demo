<?php

namespace SanyaFormDemo\Form;

use \Exception;
use DluTwBootstrap\Alert;
use SanyaForm\Form;
use SanyaFormDemo\Model\DemoModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Description of DemoForm
 *
 * @author sanyatuning
 */
class DemoForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->add(array(
            'name' => 'Text',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Text Input',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submitBtn',
            'attributes' => array(
                'value' => 'Send',
            ),
            'options' => array(
                'primary' => true,
            ),
        ));
    }

    public function setData($data) {
        if ($data instanceof DemoModel) {
            $obj = $data;
        } else {
            $obj = new DemoModel();
            $obj->exchangeArray($data);
        }
        $this->bind($obj);
    }

    protected function process() {
        $data = $this->getData();
        if ($data instanceof DemoModel) {
            $message = '<h4>Message from DemoForm->process</h4>' .
                    '<p>Input in method:</p>' .
                    '<pre>' . print_r($data->getArrayCopy(), true) . '</pre>';
            $alert = new Alert($message, Alert::TYPE_INFO);
            $this->addMessage($alert);
            if ($data->getText() == "error") {
                throw new Exception("Exception message");
            }
            return TRUE;
        }
        return FALSE;
    }

    protected function getMessage($msg_type) {
        switch ($msg_type) {
            case self::MSG_SUCCESS:
                return new Alert(parent::getMessage($msg_type), Alert::TYPE_SUCCESS);
            case self::MSG_INVALID:
                return new Alert(parent::getMessage($msg_type), Alert::TYPE_ALERT);
            case self::MSG_ERROR:
                return new Alert(parent::getMessage($msg_type), Alert::TYPE_ERROR);
        }
        return parent::getMessage($msg_type);
    }

    public function render(RendererInterface $renderer) {
        return $renderer->formtwb($this);
    }

}

