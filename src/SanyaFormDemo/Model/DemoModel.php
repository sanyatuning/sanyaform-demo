<?php

namespace SanyaFormDemo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\ArraySerializableInterface;

/**
 * Description of DemoModel
 *
 * @author sanyatuning
 */
class DemoModel implements ArraySerializableInterface, InputFilterAwareInterface {

    protected $inputFilter;
    protected $Text;

    public function __construct(array $array = array()) {
        $this->exchangeArray($array);
    }

    public function exchangeArray(array $array) {
        $data = array_merge($this->getDefaultValues(), $array);
        $this->Text = $data['Text'];
    }

    public function getArrayCopy() {
        return array(
            'Text' => $this->Text,
        );
    }

    public function getText() {
        return $this->Text;
    }

    public function setText($v) {
        $this->Text = (string) $v;
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'Text',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 3,
                                    'max' => 10,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    protected function getDefaultValues() {
        return array(
            'Text' => '',
        );
    }

}

