<?php

namespace SanyaFormDemo\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Description of DemoController
 *
 * @author sanyatuning
 */
class DemoController extends AbstractActionController {

    public function indexAction() {
        return array();
    }
    
    public function ajaxAction() {
        return array();
    }
    
    public function postAction() {
        return array();
    }
    
}
