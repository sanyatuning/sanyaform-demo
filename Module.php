<?php

namespace SanyaFormDemo;

use Zend\Navigation\Navigation;
use Zend\Navigation\Page\Mvc;

/**
 * Description of Module
 *
 * @author sanyatuning
 */
class Module {

    public function init(\Zend\ModuleManager\ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'onModuleDispatch'));
    }

    protected function setRouterForNavbar(\Zend\Navigation\AbstractContainer $navbarContainer, \Zend\Mvc\MvcEvent $e) {
        $router = $e->getRouter();
        $matches = $e->getRouteMatch();
        foreach ($navbarContainer->getPages() as $page) {
            if ($page instanceof Mvc) {
                $page->setRouter($router);
                $page->setRouteMatch($matches);
            }
        }
        return $this;
    }

    protected function getNav() {
        $config = include __DIR__ . '/config/nav.config.php';
        return new Navigation($config['navigation']['sanyaform_demo']);
    }

    public function onModuleDispatch(\Zend\Mvc\MvcEvent $e) {
        //Set the layout template for every action in this module
        $controller = $e->getTarget();
        $controller->layout('layout/layout-demo');

        //Set the main menu into the layout view model
        $navbarContainer = $this->getNav();
        $this->setRouterForNavbar($navbarContainer, $e);

        $viewModel = $e->getViewModel();
        $viewModel->setVariable('navbar', $navbarContainer);
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}

