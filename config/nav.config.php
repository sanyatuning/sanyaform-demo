<?php

return array(
    'navigation' => array(
        'sanyaform_demo' => array(
            array(
                'label' => 'Ajax',
                'route' => 'sanyaform_demo',
                'action' => 'ajax',
            ),
            array(
                'label' => 'Post/Redirect/Get',
                'route' => 'sanyaform_demo',
                'action' => 'post',
            ),
        ),
    ),
);
