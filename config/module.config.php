<?php

return array(
    'router' => array(
        'routes' => array(
            'sanyaform_demo' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/sanyaformdemo[/:action]',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'SanyaFormDemo\Controller\Demo',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'SanyaFormDemo\Controller\Demo'     => 'SanyaFormDemo\Controller\DemoController',
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/layout-demo' => __DIR__ . '/../view/layout/layout.phtml',
        ),
        'template_path_stack' => array(
            'SanyaFormDemo'    => __DIR__ . '/../view',
        ),
    ),
    'service_manager'   => array(
        'factories'         => array(
        ),
    ),
);

